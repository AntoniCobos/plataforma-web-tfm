function getTimestampInicio(){
    var hoy = new Date();
    var fecha = new Date(hoy.getFullYear(),hoy.getMonth(),hoy.getDate(),00,00,00);
    return fecha.getTime()/ 1000;
}

function getTimeStampFinal(){
    var hoy = new Date();
    var fecha = new Date(hoy.getFullYear(),hoy.getMonth(),hoy.getDate(),23,59,59);
    return fecha.getTime()/ 1000;
}

Array.prototype.rotate = (function() {
    var unshift = Array.prototype.unshift,
        splice = Array.prototype.splice;

    return function(count) {
        var len = this.length >>> 0,
            count = count >> 0;

        unshift.apply(this, splice.call(this, count % len, len));
        return this;
    };
})();

function graficareal(){
//grafica en tiempo real
var ctxL = document.getElementById("myChart1").getContext('2d');
$.ajax("getLast.php").done(function(msg) {
var res = JSON.parse(msg);
var temperatura = res["temperatura"];
var humedad = res["humedad"];
var myLineChart = new Chart(ctxL, {
    type: 'line',               // 'line', 'bar', 'bubble' and 'scatter' types are supported
    data: {
        datasets: [{
            label: "Temperatura",
            data: [],            // empty at the beginning
            backgroundColor: [
                'rgba(105, 0, 132, .2)',
            ],
            borderColor: [
                'rgba(200, 99, 132, .7)',
            ],
            borderWidth: 2
        },
        {
            label: "Huemdad",
            data: [],            // empty at the beginning
            backgroundColor: [
                'rgba(0, 137, 132, .2)',
            ],
            borderColor: [
                'rgba(0, 10, 130, .7)',
            ],
            borderWidth: 2
        }]
    },
    options: {
        scales: {
            xAxes: [{
                type: 'realtime',   // x axis will auto-scroll from right to left
                realtime: {         // per-axis options
                    /*duration: 20000,    // data in the past 20000 ms will be displayed
                    refresh: 1000, */     // onRefresh callback will be called every 1000 ms
                    delay: 200,        // delay of 1000 ms, so upcoming values are known before plotting a line
                    pause: false,       // chart is not paused
                    ttl: undefined,     // data will be automatically deleted as it disappears off the chart

                    // a callback to update datasets
                    onRefresh: function(chart) {
                        
                        chart.data.datasets.forEach(function(dataset) {
            
                          dataset.data.push({
            
                            x: Date.now(),
            
                            y: temperatura
            
                          });
                        });            
                      }
                }
            }]
        },
        
    }
});
})}

//dibuja la gráfica de la semana
function graficaSemanal(){
    var ctxL2 = document.getElementById("myChart2").getContext('2d');
    var dias = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"];
    dias.rotate(new Date().getDay()-1);
    $.ajax("getSemanario.php").done(function(msg) {
        var tempe = JSON.parse(JSON.parse(msg)[0]);
        var hume = JSON.parse(JSON.parse(msg)[1]);
        $("#cargando").remove();
        var myLineChart = new Chart(ctxL2, {
            type: 'line',
            data: {
            labels: [dias[0], dias[1], dias[2], dias[3], dias[4], dias[5], dias[6]],
                datasets: [{
                    label: "Temperatura",
                    data: [tempe[dias[0]], tempe[dias[1]], tempe[dias[2]], tempe[dias[3]], tempe[dias[4]], tempe[dias[5]], tempe[dias[6]]],
                    backgroundColor: [
                        'rgba(105, 0, 132, .2)',
                    ],
                    borderColor: [
                        'rgba(200, 99, 132, .7)',
                    ],
                    borderWidth: 2
                },
                {
                    label: "Humedad",
                    data: [hume[dias[0]], hume[dias[1]], hume[dias[2]], hume[dias[3]], hume[dias[4]], hume[dias[5]], hume[dias[6]]],
                    backgroundColor: [
                        'rgba(0, 137, 132, .2)',
                    ],
                    borderColor: [
                        'rgba(0, 10, 130, .7)',
                    ],
                    borderWidth: 2
                }
                ]
            },
            options: {
                responsive: true,
            }
        });
    })
}

//recarga la última hora de actualización
function recarga() {
    var fecha = new Date();
    getTemperaturaYHumedadActual();
    $("#estado").text(getEstado());
    getTemperaturaYHumedadMaximas();
    getTemperaturaYHumedadMinimas();
    $("#ultima-actualizacion").text(fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds());
}

//actualiza los datos
$(document).ready(function() {
    function actualizaDatosIzquierda() {
        var fecha = new Date();
        getTemperaturaYHumedadActual();
        $("#estado").text(getEstado());
        getTemperaturaYHumedadMaximas();
        getTemperaturaYHumedadMinimas();
        $("#ultima-actualizacion").text(fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds());
    }
    actualizaDatosIzquierda();
    setInterval(actualizaDatosIzquierda, 10000);
    graficaSemanal();
    //graficareal();
});

function getTemperaturaYHumedadActual(){
    $.ajax("getLast.php").done(function(msg) {
        if(msg != "KO"){
            var res = JSON.parse(msg);
            if(res["temperatura"] == null){
                $("#temperatura").text("No disponible");
            }else{
                $("#temperatura").text("\u00a0"+res["temperatura"].substring(0, 4)+"ºC");
            }
            if(res["humedad"] == null){
                $("#humedad").text("No disponible");
            }else{
                $("#humedad").text("\u00a0"+res["humedad"].substring(0, 4)+"%");
            }
        }else{
            $("#temperatura").text("\u00a0Error");
            $("#humedad").text("\u00a0Error");
        }
    }).fail(function(){
        $("#temperatura").text("\u00a0Error");
        $("#humedad").text("\u00a0Error");
    })
}
function getEstado(){
    var humedad_texto = $("#humedad").text();
    var humedad = parseInt(humedad_texto.substring(humedad_texto.length,-1),10);
    if(humedad<40){
        $("#estado").css({'color':'#990000'});
        $("#cara").attr("src","images/seco.svg");
        return "Seco ";
    }else if(humedad>60){
        $("#estado").css({'color':'#000099'});
        $("#cara").attr("src","images/humedo.svg");
        return "Húmedo ";
    }else{
        $("#estado").css({'color':'#009900'});
        $("#cara").attr("src","images/confortable.svg");
        return "Bueno ";
    }
}
function getTemperaturaYHumedadMaximas(){
    $max = getTimeStampFinal();
    $min = getTimestampInicio();
    $.ajax("getMax.php?max="+$max+"&min="+$min).done(function(msg) {
        if(msg != "KO"){
            var res = JSON.parse(msg);

            if(res["temperatura"] == null){
                $("#maximas").text("No disponible");
            }else if(res["humedad"] == null){
                $("#maximas").text("No disponible");
            }else{
                $("#maximas").text("\u00a0"+res["temperatura"].substring(0, 4)+"ºC \u00a0\u00a0"+res["humedad"].substring(0, 4)+"%");
            }            
        }else{
            $("#maximas").text("\u00a0Error");
        }
    }).fail(function(){
        $("#maximas").text("\u00a0Error");
    })
}
function getTemperaturaYHumedadMinimas(){
    $max = getTimeStampFinal();
    $min = getTimestampInicio();
    $.ajax("getMin.php?max="+$max+"&min="+$min).done(function(msg) {
        if(msg != "KO"){
            var res = JSON.parse(msg);

            if(res["temperatura"] == null){
                $("#minimas").text("No disponible");
            }else if(res["humedad"] == null){
                $("#minimas").text("No disponible");
            }else{
                $("#minimas").text("\u00a0"+res["temperatura"].substring(0, 4)+"ºC \u00a0\u00a0"+res["humedad"].substring(0, 4)+"%");
            }  
        }else{
            $("#minimas").text("\u00a0Error");
        }
    }).fail(function(){
        $("#minimas").text("\u00a0Error");
    })
}