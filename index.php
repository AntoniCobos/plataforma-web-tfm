<?php
include "header.php";
?>
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <div class="fadeIn first">
                    <span><h2>Inicio de sesión</h2></span>
                </div>
                <form method="POST" action="login.php">
                    <input type="text" id="login" class="fadeIn second" name="usuario" placeholder="nombre" maxlength="7" autocomplete="off">
                    <input type="password" id="password" class="fadeIn third" name="clave" placeholder="contraseña" maxlength="255" autocomplete="off">
                    <input type="submit" class="fadeIn fourth" value="Entrar">
                </form>
                <span class="made">Creado para el TFM de Antoni Cobos</span>
            </div>
        </div>

<?php
 include "footer.php";
?>