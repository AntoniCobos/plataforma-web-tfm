<?php
include "conectar.php";

/****Función de login ****/
function validaLogin($user,$clave){
    $opciones = [
        'cost' => 12,
    ];
    try{
        $db = getDB();
        $stmt = $db->prepare("SELECT * FROM accesos WHERE usuario=:user");
        $stmt->bindParam("user", $user,PDO::PARAM_STR) ;
        $stmt->execute();
        $count=$stmt->rowCount();
        $data=$stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
        if($count){
            if(password_verify($clave, $data->clave)){
                session_start();
                $_SESSION["usuario"] = $data->usuario;
                return true;
            }else{
                return false;
            }
        }
        else{
            return false;
        } 
    }
    catch(PDOException $e) {
        return false;
    }
}   

function getMediaTemperatura($min,$max){
    try{
        $db = getDB();
        $stmt = $db->prepare("SELECT AVG(temperatura) as temperatura FROM valores WHERE fecha >= ".$min." AND fecha <= ".$max);
        $stmt->execute();
        $count=$stmt->rowCount();
        $data=json_encode($stmt->fetch(PDO::FETCH_OBJ));
        $db = null;
        if($count){
            return (int)explode("\"",$data)[3];
            //return $data["temperatura"];
        }else{
            return 0;
        } 
    }
    catch(PDOException $e) {
        return $e;
    }
}

function getMediaHumedad($min,$max){
    try{
        $db = getDB();
        $stmt = $db->prepare("SELECT AVG(humedad) as humedad FROM valores WHERE fecha >= ".$min." AND fecha <= ".$max);
        $stmt->execute();
        $count=$stmt->rowCount();
        $data=json_encode($stmt->fetch(PDO::FETCH_OBJ));
        $db = null;
        if($count){
            return (int)explode("\"",$data)[3];
        }else{
            return 0;
        } 
    }
    catch(PDOException $e) {
        return $e;
    }
}

?>