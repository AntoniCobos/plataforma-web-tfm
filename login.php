<?php
include "funciones.php";
if(isset($_POST['usuario']) && isset($_POST['clave'])){ //Viene del login
    $user = $_POST['usuario'];
    $clave = $_POST['clave'];
    if(validaLogin($user,$clave)){//Comprobamos si existe el usuario
        session_start();
        $_SESSION["nombre"] = $user;
        header('location: panel.php');
    }else{
        header('location: index.php');
    }
}else{
    header('location: index.php');
}

?>