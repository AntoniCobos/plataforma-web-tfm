<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Plataforma UBUTemp</title>
        <link rel="stylesheet" href="css/style.css" />
        <script type="text/javascript" src="assets/js/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/moment.js"></script>
        <script type="text/javascript" src="assets/js/Chart.js"></script>
        <script type="text/javascript" src="assets/js/chartjs-plugin-streaming.min.js"></script>
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
    </head>
    <body>