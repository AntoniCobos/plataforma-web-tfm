<?php
session_start();
if(isset($_SESSION["usuario"]) != 1){
    header('location: logout.php');
    echo "No tienes permiso para ver esta página...";
    exit();
}
include "header.php";
include "funciones.php";
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<img alt="icono para cerrar sesión" src="images/power-off-solid.svg" class="salir" onclick="window.location.href='logout.php'"/>
		</div>
    </div>
	<!--<div class="row justify-content-center">
        <div class="col-md-12">
        <h1 class="text-center">Temperatura y humedad en tiempo real</h1><br>
        <canvas id="myChart1" width="100%" height="15"></canvas>
        </div>
      </div>-->
      <br>
	<div class="row align-items-center d-flex align-items-stretch">
		<div class="col-md-3 border border-secondary mx-4 px-3 py-5">
            <h3 class="text-left espacio">Temperatura:   <span class="font-weight-bold" id="temperatura"></span></h3>
            <h3 class="text-left espacio">Humedad:   <span class="font-weight-bold" id="humedad"></span></h3>
            <h3 class="text-left espacio">Estado del aire: <span class="font-weight-bold" id="estado"> </span><img id="cara" src="#" class="caras"/></h3>
            <h3 class="text-left espacio">Máximas:   <span class="font-weight-bold" id="maximas"></span></h3>
            <h3 class="text-left">Mínimas:   <span class="font-weight-bold" id="minimas"></span></h3>
            <span class="float-right abajo-derecha">Última actualización: <span id="ultima-actualizacion"></span> <img alt="icono para cerrar sesión" src="images/sync-solid.svg" class="reload" onclick="recarga()"/></span>
		</div>
		<div class="col-md-8 border border-secondary">
            <h1 class="text-left">Última semana</h1><br>
            <h3 class="text-center" id="cargando">Cargando...</h3>
            <canvas id="myChart2" width="100%" height="30">Cargando...</canvas>
		</div>
      </div><br>
      <div class="row justify-content-center">
            <div class="col-md-12">
            <a class="weatherwidget-io" href="https://forecast7.com/en/40d71n74d01/new-york/" data-label_1="BURGOS" data-label_2="Tiempo" data-icons="Climacons Animated" data-theme="weather_one" >BURGOS Tiempo</a>
            <script>
                  !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
            </script>
            </div>
      </div>
</div>
<script type="text/javascript" src="assets/js/funciones.js"></script>
<?php
 include "footer.php";
?>