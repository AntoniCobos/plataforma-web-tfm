<?php
include "funciones.php";
if (isset($_GET["max"]) && isset($_GET["min"])) {
    try{
        $max = 
        $db = getDB();
        $stmt = $db->prepare("SELECT MIN(temperatura) as temperatura, MIN(humedad) as humedad FROM valores WHERE fecha >= ".$_GET['min']." AND fecha <= ".$_GET['max']);
        $stmt->execute();
        $count=$stmt->rowCount();
        $data=json_encode($stmt->fetch(PDO::FETCH_OBJ));
        $db = null;
        if($count){
            echo $data;
        }else{
            echo "KO";
        } 
    }
    catch(PDOException $e) {
        echo "KO";
    }
}else{
    echo "KO";
}
?>