<?php
include "funciones.php";
try{
    $db = getDB();
    $stmt = $db->prepare("SELECT * FROM valores WHERE fecha = (SELECT MAX(fecha) FROM valores)");
    $stmt->execute();
    $count=$stmt->rowCount();
    $data=json_encode($stmt->fetch(PDO::FETCH_OBJ));
    $db = null;
    if($count){
        echo $data;
    }else{
        echo "KO";
    } 
}
catch(PDOException $e) {
    echo "KO";
}
?>